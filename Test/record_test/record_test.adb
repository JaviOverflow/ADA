with Ada.Text_IO;
use Ada.Text_IO;

procedure record_test is
    type cosa is (tiempo, fruta, otro);

    type dato;
    type pdato is access dato;

    type dato(t: cosa:= tiempo) is
        record
            case t is
                when tiempo =>
                    prep: float;
                when fruta =>
                    kind: character;
                    kg: integer;
                when otro =>
                    c: character;
                    n: pdato;
            end case;
        end record;

    a,b,c,d,e,f: dato;
    p: pdato;
    x: boolean;
    n1: integer := 200;
    n2: integer := integer(float(n1)/0.8);
begin
    a:= (tiempo, 89.2);
    b:= (tiempo, 89.2);
    c:= (fruta, 'a', 89);
    d:= (tiempo, 78.2);

    p:= new dato'(tiempo, 89.2);
    e:= (otro, 'e', p);
    f:= (otro, 'e', p);
    
    put(integer'image(n2));

    x:= a=b; -- True
    put(boolean'image(x));
    
    x:= a=c; -- False
    put(boolean'image(x));

    x:= a=d; -- False
    put(boolean'image(x));

    x:= e=f; -- True
    put(boolean'image(x));
end record_test;
