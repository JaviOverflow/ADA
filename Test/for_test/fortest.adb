with Ada.Text_IO;
use Ada.Text_IO;

procedure fortest is
    type index is new positive;
    n: index;
begin
    n:= 2;
    for i in 1..n loop put(index'image(n)); end loop;
end fortest;
