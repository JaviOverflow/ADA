package body d_rules is

    procedure empty(rt: out rules_table) is
        n: index renames rt.n;
    begin
        n:= 0;
    end empty;

    procedure put(rt: in out rules_table) is
        n: index renames rt.n;
        m: mem_space renames rt.m;
        e1, e2: expression;
    begin
        read_rule(e1, e2);
        n:= n+1; m(n):= (e1, e2);
    exception
        when constraint_error => raise space_overflow;
    end put;

    function to_string(rt: in rules_table) return string is
        nl: string:= ada.characters.latin_1.CR & ada.characters.latin_1.LF;
        n: index renames rt.n;
        m: mem_space renames rt.m;
        buffer: unbounded_string;
    begin
        if n=0 then
            append(buffer, "There are no rules");
        else
            for i in 1..n loop
                append(buffer, index'image(i) & ".- " & to_string(m(i).e1) 
                                & " = " & to_string(m(i).e2) & nl);
            end loop;
        end if;
        return to_string(buffer);
    end to_string;

    function apply_rule(rt: in rules_table; r: in natural; it: in out iterator) 
            return expression is
        gen1: expression renames rt.m(index(r)).e1;
        gen2: expression renames rt.m(index(r)).e2;
    begin
        return build_expression_from_generics(gen1, gen2, it);
    end apply_rule;
end d_rules;
