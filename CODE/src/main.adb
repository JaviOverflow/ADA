-- Expression treatment packages
with d_rules, d_stack, utilities;   use utilities;
with global_declarations;           use global_declarations;
                                    use global_declarations.my_dexpr;

-- Input / Output imports
with ada.text_io, ada.characters.latin_1, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

-- System imports
with ada.command_line;
use ada.command_line;

procedure main is
    -- Menu variables
    nl: string:= ada.characters.latin_1.CR & ada.characters.latin_1.LF;
    clear_screen: string:= ascii.esc & "[2J";
    x: character; -- character to hold user option

    package my_drules is new d_rules(max_rules); use my_drules;
    package my_dstack is new d_stack(expression, max_expression_changes); 
    use my_dstack;
    -- Expression related variables
    e: expression;
    rt: rules_table;
    s: stack;

    -- Reads new expression
    function new_expression return boolean is
    begin
        put_line("Expression will be read from file: " & argument(1));
        read_e(argument(1), e);
        empty(rt);

        put_line("Expression was succesfully read from file.");
        return true;
    exception
        when syntax_error =>
            put_line("ERROR: File does not contains a correct expression.");
            return false;
    end new_expression;

    -- Output the expression inserted
    procedure show_expression is
        it: iterator;
    begin
        first(e, it);
        if is_valid(it) then put_line("The expression is: " & to_string(e));
                        else put_line("There isn't any valid expression stored");
        end if;
    end show_expression;

    -- Adds new rule
    procedure new_rule is
    begin
        put("Enter the desired rule: ");
        put(rt);
        put_line(nl & "Rule was succesfully inserted.");
    exception
        when syntax_error =>
            put_line("Rule does not have a correct syntax.");
        when my_drules.space_overflow =>
            put_line("No left space for more rules.");
    end new_rule;

    procedure simplify_expression is
        x: character;
        it: iterator;
        r: natural;
        new_e: expression;
    begin
        first(e, it);

        -- Output rules
        put_line("The available rules are:" & nl & to_string(rt));

        -- Choosing the subexpression
        put("Choose the subexpression you want to simplify."
                & nl & "  [y] choose current subexpression"
                & nl & "  [n] go to the next subexpression" 
                & nl & "  [m] go back to menu"
                & nl
                & nl & "Current subexpression" & to_string(it)
                & nl & "Option: "); get(x); put(nl);
        while x/='y' loop
            case x is
                -- Subexpression selected
                when 'y' => null;
                -- Next expression
                when 'n' =>
                    succ(it);
                    if not is_valid(it) then first(e, it); end if;
                    put(  nl & "Current subexpression => " & to_string(it)
                        & nl & "Option: "); get(x);
                -- Back to menu
                when 'm' =>
                    put_line("Going back to menu...");
                    return;
                -- Invalid option
                when others =>
                    put_line("Select a valid option please!");
                    put(  nl & "Current subexpression" & to_string(it)
                        & nl & "Option: "); get(x);
            end case;
        end loop;

        -- Ask for apply a rule or simplify
        while not (x='r' or x='o') loop
            put("Do you want to apply a rule[r] or operate[o]?" & nl 
                & "Option: "); get(x); put(nl);
            case x is
                when 'r' =>
                    put("Enter the rule number you want to apply: "); get(r);

                    new_e:= apply_rule(rt, r, it);
                    if e_type(new_e)=e_null then 
                        put("The rule selected cannot be applied to the"
                            & "current subexpression!");
                    else 
                        -- Save last expression and update with the new one
                        push(s, e); e:= new_e; 
                        put("Applied! The new expression is => " 
                            & to_string(e)); put(nl);
                    end if;

                when 'o' =>
                    new_e:= operate(it);
                    if e_type(new_e)=e_null then 
                        put_line("That subexpression cannot be operated");
                    else
                        -- Save last expression and update with the new one
                        push(s, e); e:= new_e;
                        put("Operated!" & nl & "The new expression is => " 
                            & to_string(e)); put(nl);
                    end if;
                when others =>
                    put_line("Select a valid option please!");
                    put("Do you want to apply a rule[r] or operate[o]?" 
                        & nl & "Option: "); get(x); put(nl);
            end case;
        end loop;
    end simplify_expression;

    procedure undo is
    begin
        if is_empty(s) then
            put_line("All changes were undone already. Cannot keep undoing.");
        else 
            e:= top(s); pop(s); 
            put("The new expression is => " & to_string(e)); put(nl);
        end if;
    end undo;
begin
    -- If file path isn't introduced as argument, end execution
    if argument_count=0 then 
        put_line("ERROR: File path is required"); 
        return; 
    end if;

    -- Init the expression historial stack
    empty(s);

    -- If expression read is not valid, end execution
    if not new_expression then return; end if;

    x:= '@';
    while x/='0' loop
        -- Output the menu info
        put(nl & nl & "--------------------------------------------------------");
        put(  nl & "[1] Show expression"
            & nl & "[2] New rule"
            & nl & "[3] Simplify the expression"
            & nl & "[4] Undo last change"
            & nl & "[0] Quit"
            & nl & "Enter one of the above numbers to choose an action: ");

        -- Read the user input
        get(x); put(nl);
        case x is
            when '0' => put_line("Quitting...");
            when '1' => show_expression;
            when '2' => new_rule;
            when '3' => simplify_expression;
            when '4' => undo;
            when others => put_line("Please insert a correct option");
        end case;
    end loop;
end main;
