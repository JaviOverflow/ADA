generic
    type elem is private;
    n: natural;
package d_stack is

    type stack is limited private;

    space_overflow, bad_use: exception;

    procedure empty (s: out stack);
    procedure push (s: in out stack; x: in elem);
    procedure pop (s: in out stack);
    function is_empty (s: in stack) return boolean;
    function top (s: in stack) return elem;

    function "=" (s1, s2: in stack) return boolean;

private
    max: constant natural:= n;
    type index is new natural range 0..max;
    type mem_space is array(index range 1..index'last) of elem;

    type stack is
        record
            m: mem_space;
            n: index;
        end record;
end d_stack;


