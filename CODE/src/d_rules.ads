with utilities;             use utilities;
with global_declarations;   use global_declarations, global_declarations.my_dexpr;
with ada.strings.unbounded; use ada.strings.unbounded;
with ada.characters.latin_1; 

generic
    n: natural;
package d_rules is
    type rules_table is limited private;

    space_overflow: exception;
    does_not_exists: exception;

    procedure empty(rt: out rules_table);
    procedure put(rt: in out rules_table);
    function to_string(rt: in rules_table) return string;

    function apply_rule(rt: in rules_table; r: in natural; it: in out iterator) 
        return expression;
private
    max_r: constant natural:= n;

    type index is new natural range 0..max_r;

    type block is
        record
            e1: expression;
            e2: expression;
        end record;

    type mem_space is array(index range 1..index'last) of block;

    type rules_table is
        record
            m: mem_space;
            n: index;
        end record;
end d_rules; 
