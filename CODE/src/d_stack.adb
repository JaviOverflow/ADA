package body d_stack is

    procedure empty (s: out stack) is
        n: index renames s.n;
    begin
        n:= 0;
    end empty;

    procedure push (s: in out stack; x: in elem) is
        n: index renames s.n;
        m: mem_space renames s.m;
    begin
        if n=index(max) then raise space_overflow; end if;
        n:= n+1; m(n):= x;
    end push;


    procedure pop (s: in out stack) is
        n: index renames s.n;
    begin
        if n=index(0) then raise bad_use; end if;
        n:= n-1;
    end pop;
        
    function is_empty (s: in stack) return boolean is
        n: index renames s.n;
    begin
        return n=0;
    end is_empty;


    function top (s: in stack) return elem is
        n: index renames s.n;
        m: mem_space renames s.m;
    begin
        return m(n);
    end top;

    -- Function that test equality between two stacks
    function "=" (s1, s2: in stack) return boolean is
        n1: index renames s1.n;
        m1: mem_space renames s1.m;
        n2: index renames s2.n;
        m2: mem_space renames s2.m;
        i: index;
        i0: index:= index'first + 1;
    begin
        -- If their length are not equal, return false
        if not (n1=n2) then return false; end if;      

        -- If both are empty, return true
        if is_empty(s1) and is_empty(s2) then return true; end if;

        -- Traversal of the memory space looking for a different element
        i:= n1;
        while i > i0 and then m1(i)=m2(i) loop i:= i-1; end loop;

        -- If they all are the same, 0 index will be reached
        return i=i0 and then m1(i0)=m2(i0);
    end "=";

end d_stack;
