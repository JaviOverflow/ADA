with ada.strings;              use ada.strings;
with ada.strings.fixed;        use ada.strings.fixed;
with ada.strings.unbounded;    use ada.strings.unbounded;

package body d_expr is
    subtype index is expression;

    type block(et: expression_type:= e_null) is
        record
            case et is
                when e_null =>
                    null;
                when e_const =>
                    val: natural;
                when e_var =>
                    var: character;
                when e_un =>
                    opun: un_op;
                    esb: index;
                when e_bin =>
                    opbin: bin_op;
                    esb1, esb2: index;
                when e_gen =>
                    gen: character;
                end case;
            end record;

    type block_state is (occupied, free);

    type cell is
        record
            b: block;
            bs: block_state;
        end record;

    type expression_space is array(index) of cell;

    type expression_table is
        record
            m: expression_space;
            n: natural;
        end record;

    et: expression_table;

    -- Equal test
    function generic_equal(e1, e2: in expression) return boolean is
        b1: block renames et.m(e1).b;
        b2: block renames et.m(e2).b;
        r: boolean;
    begin
        r:= false;
        if b1.et=b2.et then
            case b1.et is
                when e_un =>
                    r:= b1.opun=b2.opun;
                when e_bin =>
                    r:= b1.opbin=b2.opbin;
                when others => 
                    r:= e1=e2;
            end case;
        end if;
        return r;
    end generic_equal;

    -- Hashing declarations
    type vector is array(1..9) of natural;
    m: constant natural:= 256;

    -- Hashing procedures and functions
    procedure append(v0: in natural; a: in out vector; n: in out natural) is
        v: natural;
    begin
        v:= v0;
        while v>0 loop
            n:= n+1; a(n):= v mod m; v:= v/m;
        end loop;
    end append;

    procedure to_vector(b: in block; a: out vector; n: out natural) is
    begin
        n:= 1;
        a(1):= expression_type'pos(b.et);
        case b.et is
            when e_null => null;
            when e_const => append(b.val, a, n);
            when e_var => append(character'pos(b.var), a, n);
            when e_gen => append(character'pos(b.gen), a, n);
            when e_un =>
                a(1):= a(1)*8 + un_op'pos(b.opun);
                append(natural(b.esb), a, n);
            when e_bin =>
                a(1):= a(1)*8 + bin_op'pos(b.opbin);
                append(natural(b.esb1), a, n);
                append(natural(b.esb2), a, n);
        end case;
    end to_vector;

    procedure to_natural(a: in vector; n: in natural; t: out natural) is
        c: natural;
        k, l: integer;
        r: array(1..2*n) of natural;
    begin
        for i in 1..2*n loop r(i):= 0; end loop;
        k:= 2*n+1;
        for i in reverse 1..n loop
            k:=k-1; l:= k; c:= 0;
            for j in reverse 1..n loop
                c:= c + r(l) + a(i)*a(j);
                r(l):= c mod m; c:= c/m;
                l:= l-1;
            end loop;
            r(l):= c;
        end loop;
        t:= (r(n)*m + r(n+1));
    end to_natural;

    function hash(bl: in block) return index is
        a: vector; n: natural; t: natural;
    begin
        to_vector (bl, a, n);
        to_natural(a, n, t);
        return index(t mod b);
    end hash;

    procedure allocate(b: in block; e: out index) is
        m: expression_space renames et.m;
        n: natural renames et.n;
        i0, i: index; -- initial and current position
        na: index; -- number of attempts
    begin
        if n=max_e then raise space_overflow; end if;

        i0:= hash(b); na:= 0; i:= i0;
        while m(i).bs/=free and m(i).b/=b loop
            na:= na+1; i:= i0+na*na;
        end loop;

        if m(i).bs=free then m(i):= (b, occupied); end if;
        e:= i;
    end allocate;

    procedure prep_expr_space is
        m: expression_space renames et.m;
        n: natural renames et.n;
    begin
        for i in index'range loop m(i).bs:= free; end loop;
        n:= 0;
    end prep_expr_space;

    function b_null return expression is
        b: block; e: index;
    begin
        b:= block'(et=>e_null);
        allocate(b,e);
        return e;
    end b_null;

    function b_constant(n: in natural) return expression is
        b: block; e: index;
    begin
        b:= (e_const, n);
        allocate(b,e);
        return e;
    end b_constant;

    function b_var(x: in character) return expression is
        b: block; e: index;
    begin
        b:= (e_var,x);
        allocate(b,e);
        return e;
    end b_var;

    function b_gen(x: in character) return expression is
        b: block; e: index;
    begin
        b:= (e_gen,x);
        allocate(b,e);
        return e;
    end b_gen;

    function b_un_op (op: in un_op; esb: in expression) return expression is
        b: block; e: index;
    begin
        b:= (e_un,op,esb);
        allocate(b,e);
        return e;
    end b_un_op;

    function b_bin_op(op: in bin_op; e1,e2: in expression) return expression is
        b:block; e: index;
    begin
        b:= (e_bin,op,e1,e2);
        allocate(b,e);
        return e;
    end b_bin_op;

    function e_type(e: in expression) return expression_type is
        s: expression_space renames et.m;
    begin
        return s(e).b.et;
    end e_type;

    function g_const(e: in expression) return natural is
        s: expression_space renames et.m;
    begin
        return s(e).b.val;
    end g_const;

    function g_var(e: in expression) return character is
        s: expression_space renames et.m;
    begin
        return s(e).b.var;
    end g_var;

    function g_gen(e: in expression) return character is
        s: expression_space renames et.m;
    begin
        return s(e).b.gen;
    end g_gen;

    procedure g_un(e: in expression; op: out un_op; esb: out expression) is
        s: expression_space renames et.m;
    begin
        op:= s(e).b.opun;
        esb:= s(e).b.esb;
    end g_un;

    procedure g_bin(e: in expression; op: out bin_op; esb1, esb2: 
            out expression) is
        s: expression_space renames et.m;
    begin
    op:= s(e).b.opbin;
    esb1:= s(e).b.esb1;
    esb2:= s(e).b.esb2;
    end g_bin;

    -- Iterator procedures and functions

    function "=" (it1, it2: in iterator) return boolean is
        ps1: stack renames it1.path;
        ps2: stack renames it2.path;
        e1: expression renames it1.sub_e;
        e2: expression renames it2.sub_e;
    begin
        return ps1=ps2 and e1=e2;
    end "=";

    procedure first (e: in expression; it: out iterator) is
        m: expression_space renames et.m;
        it_e: expression renames it.e;
        ps: stack renames it.path;
        i: expression renames it.sub_e;
    begin
        -- Save root expression
        it_e:= e;

        -- Init stack and current subexpression
        empty(ps); i:= e;

        -- While it has left child
        while e_type(i)=e_bin or e_type(i)=e_un loop
            -- Save path
            push(ps, (i, left));
            -- Update current subexpression
            case m(i).b.et is
                when e_un =>    i:= m(i).b.esb;
                when e_bin =>   i:= m(i).b.esb1;
                when others =>  null; -- never reached
            end case;
        end loop;
    end first;

    function is_valid (it: in iterator) return boolean is
        i: expression renames it.sub_e;
    begin
        return e_type(i)/=e_null;
    end is_valid;

    procedure get (it: in iterator; e: out expression) is
        i: expression renames it.sub_e;
    begin
        if e_type(i)/=e_null then e:= i;
                             else raise bad_use;
        end if;
    end get;
        

    procedure succ (it: in out iterator) is
        m: expression_space renames et.m;
        ps: stack renames it.path;
        i: expression renames it.sub_e;

        p: path;
    begin
        -- If subexpression is null, raise bad use
        if e_type(i)=e_null then raise bad_use; end if;

        -- If sub_e has right child, explore right subtree
        if e_type(i)=e_bin then
            -- Save path and update sub_e
            push(ps, (i, right)); i:= m(i).b.esb2;

            -- While it has left child
            while e_type(i)=e_bin or e_type(i)=e_un loop
                -- Save path
                push(ps, (i, left));
                -- Update current subexpression
                case m(i).b.et is
                    when e_un =>    i:= m(i).b.esb;
                    when e_bin =>   i:= m(i).b.esb1;
                    when others =>  null; -- never reached
                end case;
            end loop;
        -- If right sub_tree does not exists, go up until we find an 
        --      expression non visited
        else
            p:= (b_null, right);
            while p.d/=left loop
                if not is_empty(ps) then p:= top(ps); pop(ps); 
                                    else p:= (b_null, left);
                end if;
            end loop;
            i:= p.e;
        end if;
    end succ;


    function to_string (it0: in iterator; highlight_subexpression: in boolean) 
            return string;
    procedure to_string0 (it0: in iterator; hl: in boolean; 
        it: in out iterator; buffer: in out unbounded_string);
    procedure append (buffer: in out unbounded_string; op: in un_op);
    procedure append (buffer: in out unbounded_string; op: in bin_op);
    
    function to_string(it: in iterator) return string is
    begin
        return to_string(it, true);
    end to_string;

    function to_string(e: in expression) return string is
        it: iterator;
    begin
        first(e, it);
        return to_string(it, false);
    end to_string;


    -- Prints the expression related to the iterator 'it', highlighting with 
    --  square brackets a certain subexpression (depending on iterator state) 
    --  when 'hl' is true. If it's false, clean expression will be printed.
    function to_string (it0: in iterator; highlight_subexpression: in boolean) 
            return string is
        it: iterator;
        buffer: unbounded_string;
    begin
        -- Init recursive call variables
        empty(it.path); it.sub_e:= it0.e;

        -- Generate unbounded string with expression
        to_string0 (it0, highlight_subexpression, it, buffer);

        -- Return bounded string
        return to_string(buffer);
    end to_string;

    -- Auxiliar procedure for recursive calls
    procedure to_string0 (it0: in iterator; hl: in boolean; 
        it: in out iterator; buffer: in out unbounded_string) is

        hl_this: boolean; -- True when this is the expression to highlight

        ps: stack renames it.path;
        i: expression renames it.sub_e;

        p: path;

        -- Variables for unary and binary expression cases
        e1, e2: expression;
        opu: un_op; 
        opb: bin_op;
    begin
        -- Check if subexpression must be highlighted and it's this one
        if hl 
            then hl_this:= it0=it;
            else hl_this:= false;
        end if;
        if hl_this then append(buffer, "["); end if;

        -- Append expression info
        case e_type(i) is
            when e_null =>
                append(buffer, "Invalid expression");

            when e_const =>
                append(buffer, trim(natural'image(g_const(i)), left));

            when e_var =>
                append(buffer, g_var(i));

            when e_gen =>
                append(buffer, g_gen(i));

            when e_un =>
                -- Save path and update current sub_e
                push(ps, (i, left)); g_un(i, opu, i);
                
                if opu=neg then append(buffer, "("); end if;
                append(buffer, opu); -- Append operand

                -- Append subexpression
                if e_type(i)/=e_bin then append(buffer, "("); end if;
                to_string0(it0, hl, it, buffer);
                if e_type(i)/=e_bin then append(buffer, ")"); end if;

                if opu=neg then append(buffer, ")"); end if;
            when e_bin =>
                g_bin(i, opb, e1, e2);

                if not hl_this then append(buffer, "("); end if;

                push(ps, (i, left)); i:= e1;
                to_string0(it0, hl, it, buffer); 

                append(buffer, opb);

                push(ps, (i, right)); i:= e2;
                to_string0(it0, hl, it, buffer); 

                if not hl_this then append(buffer, ")"); end if;
        end case;

        -- If this is the subexpression to hl, append ending square bracket
        if hl_this then append(buffer, "]"); end if;

        -- Pop subexpression since it's already processed
        if not is_empty(ps) then p:= top(ps); pop(ps); i:= p.e;
                            else i:= b_null;
        end if;

    end to_string0;

    -- Append to the string the unary operand
    procedure append (buffer: in out unbounded_string; op: in un_op) is
    begin
        case op is
            when neg => append(buffer, "-");
            when exp | sin | cos | log => append(buffer, un_op'Image(op));
        end case;
    end append;

    -- Append to the string the binary operand
    procedure append (buffer: in out unbounded_string; op: in bin_op) is
    begin
        case op is
            when add => append(buffer, "+");
            when sub => append(buffer, "-");
            when prod => append(buffer, "*");
            when quot => append(buffer, "/");
            when power => append(buffer, "^");
        end case;
    end append;

    ------------------ EXPRESSION BUILDS FROM GENERICS ------------------------
    function is_generic_analog(exp, gen: in expression; m: out map) 
            return boolean;
    function build_expression_from_generic(g: in expression; m: in map) 
            return expression;
    function grow_expression_with_iterator(it: in out iterator; 
            e: in expression) return expression;

    function build_expression_from_generics(gen1, gen2: in expression; 
            it: in out iterator) return expression is
        e: expression;
        m: map;
    begin
        get(it, e);
        empty(m);
        if is_generic_analog(e, gen1, m) then
            e:= build_expression_from_generic(gen2, m);
            if e_type(e)/=e_null 
                then return grow_expression_with_iterator(it, e);
                else return b_null;
            end if;
        else
            return b_null;
        end if;
    end build_expression_from_generics;

    function is_generic_analog(exp, gen: in expression; m: out map) 
            return boolean is
        e1, e2, g1, g2: expression;
        uop: un_op;
        bop: bin_op;
        r: boolean;
    begin
        r:= false;

        if generic_equal(exp, gen) then
            case e_type(exp) is
                when e_un =>
                    g_un(exp, uop, e1);
                    g_un(gen, uop, g1);
                    if is_generic_analog(e1, g1, m) then r:= true; end if;
                when e_bin =>
                    g_bin(exp, bop, e1, e2);
                    g_bin(gen, bop, g1, g2);
                    if is_generic_analog(e1, g1, m) and then 
                        is_generic_analog(e2, g2, m) then 
                        r:= true; 
                    end if;
                when others =>
                    r:= true;
            end case;
        else
            if e_type(gen)=e_gen then put(m, g_gen(gen), exp); r:= true; end if;
        end if;

        return r;
    exception
        when my_dmap.already_exists => return false;
    end is_generic_analog;


    function build_expression_from_generic(g: in expression; m: in map) 
            return expression is
        e1, e2: expression;
        new_e: expression;
        g1, g2: expression;
        uop: un_op;
        bop: bin_op;
    begin
        case e_type(g) is
            when e_un =>
                g_un(g, uop, g1);
                e1:= build_expression_from_generic(g1, m);
                if e_type(e1)/=e_null then new_e:= b_un_op(uop, e1);
                                      else new_e:= b_null;
                end if;
            when e_bin =>
                g_bin(g, bop, g1, g2);
                e1:= build_expression_from_generic(g1, m);
                e2:= build_expression_from_generic(g2, m);
                if e_type(e1)/=e_null and e_type(e2)/=e_null 
                    then new_e:= b_bin_op(bop, e1, e2);
                    else new_e:= b_null;
                end if;
            when e_gen =>
                new_e:= get(m, g_gen(g));
            when others =>
                new_e:= g;
        end case;
        return new_e;
    exception
        when my_dmap.does_not_exists => return b_null;
    end build_expression_from_generic;

    function grow_expression_with_iterator(it: in out iterator; 
            e: in expression) return expression is
        ps: stack renames it.path;
        p: path;

        uop: un_op;
        bop: bin_op;
        e1, e2: expression;

        new_e: expression;
    begin
        new_e:= e;
        while not is_empty(ps) loop
            p:= top(ps); pop(ps);
            case e_type(p.e) is
                when e_un =>
                    g_un(p.e, uop, e1);
                    new_e:= b_un_op(uop, new_e);
                when e_bin =>
                    g_bin(p.e, bop, e1, e2);
                    if p.d=left then new_e:= b_bin_op(bop, new_e, e2);
                                else new_e:= b_bin_op(bop, e1, new_e);
                    end if;
                when others =>
                    null; -- Never reached. Only unary and binary op has childs
            end case;
        end loop;
        return new_e;
    end grow_expression_with_iterator;

    ------------------- OPERATE -------------------------------
    function operate(e: in expression) return expression;

    function operate(it: in out iterator) return expression is
        e: expression;
    begin
        get(it, e); 
        e:= operate(e);
        if e_type(e)=e_null then return b_null;
                            else return grow_expression_with_iterator(it, e);
        end if;
    end operate;

    function operate(e: in expression) return expression is
        new_e: expression;

        bop: bin_op;
        e1, e2: expression;

        i: integer;
    begin
        case e_type(e) is
            when e_const =>
                new_e:= e;
            when e_bin =>
                g_bin(e, bop, e1, e2);
                e1:= operate(e1); e2:= operate(e2);
                if e_type(e1)=e_const and e_type(e2)=e_const then
                    case bop is
                        when add =>
                            new_e:= b_constant(g_const(e1) + g_const(e2));
                        when sub =>
                            i:= g_const(e1) - g_const(e2);
                            if i<0 then 
                                new_e:= b_constant(natural(-i));
                                new_e:= b_un_op(neg, new_e);
                            else
                                new_e:= b_constant(natural(i));
                            end if;
                        when prod =>
                            new_e:= b_constant(g_const(e1) * g_const(e2));
                        when quot =>
                            new_e:= b_constant(g_const(e1) / g_const(e2));
                        when power =>
                            new_e:= b_constant(g_const(e1) ** g_const(e2));
                    end case;
                else
                    new_e:= b_null;
                end if;
            when others =>
                new_e:= b_null;
        end case;
        return new_e;

    end operate;
begin
    prep_expr_space;
end d_expr;
