generic
    type key is (<>);
    type item is private;
package d_map is
    type map is limited private;

    already_exists, does_not_exists: exception;

    procedure empty (m: out map);
    procedure put (m: in out map; k: in key; x: in item);
    function get (m: in map; k: in key) return item;
private
    type existence is array(key) of boolean;
    type contents is array(key) of item;

    type map is
        record
            e: existence;
            c: contents;
        end record;
end d_map;
