with ada.strings.unbounded;    use ada.strings.unbounded;

package body utilities is

    c: character; -- Next character to process
    f: file_type; -- File containing the expression
    reading_rule: boolean:= false; -- if true, uses b_gen for literals. 
                                   -- Otherwise, b_var.

    procedure read_expr (e: out expression);
    procedure read_term (e: out expression);
    procedure read_factor (e: out expression);
    procedure read_factor1 (e: out expression);
    function scan_id   return un_op;
    function scan_num return natural;

    procedure next_char is
    begin
        if reading_rule then get(c); 
                        else get(f,c); 
        end if;
    end next_char;

    procedure read_expr(e: out expression) is
        neg_term: boolean;
        op:		  character;
        e1:		  expression;
    begin
        neg_term:= c='-'; if neg_term then next_char; end if;
        read_term(e);
        if neg_term then e:= b_un_op(neg, e); end if;
        while c='+' or c='-' loop
            op:= c; next_char; read_term(e1);
            if op='+' then e:= b_bin_op(add, e, e1);
                      else e:= b_bin_op(sub, e, e1);
            end if;
        end loop;
    end read_expr;

    procedure read_term(e: out expression) is
        op: character;
        e1: expression;
    begin
        read_factor(e);
        while c='*' or c='/' loop
            op:= c; next_char; read_factor(e1);
            if op ='*' then e:= b_bin_op(prod, e, e1);
                       else e:= b_bin_op(quot, e, e1);
            end if;
       end loop;
    end read_term;

    procedure read_factor(e: out expression) is 
        e1: expression;
    begin
        read_factor1(e);
        if c='^' then
            next_char; read_factor1(e1);
            e:= b_bin_op(power, e, e1);
        end if;
    end read_factor;

    procedure read_factor1(e: out expression) is
        t: un_op;
        x: character;
    begin
        if c='(' then
            next_char; read_expr(e);
            if c/=')' then raise syntax_error; end if;
            next_char;
        elsif 'a'<=c and c<='z' then
            x:= c;
            t:= scan_id; 
            if t=neg then 
                if reading_rule then e:= b_gen(x);
                                else e:= b_var(x); 
                end if;
            else
                if c/='(' then raise syntax_error; end if;
                next_char;
                read_expr(e);
                if c/=')' then raise syntax_error; end if;
                next_char;
                e:= b_un_op(t, e);
            end if;
        elsif '0'<=c and c<='9' then
            e:= b_constant(scan_num);
        else
            raise syntax_error;
        end if;
    end read_factor1;

    -- (log, sin, cos, exp)
    function scan_id return un_op is
    begin
        case c is
            when 'l' =>
                next_char; if c/='o' then return neg; end if;
                next_char; if c/='g' then raise syntax_error; end if;
                next_char; return log;

            when 's' =>
                next_char; if c/='i' then return neg; end if;
                next_char; if c/='n' then raise syntax_error; end if;
                next_char; return sin;

            when 'c' =>
                next_char; if c/='o' then return neg; end if;
                next_char; if c/='s' then raise syntax_error; end if;
                next_char; return cos;

            when 'e' => 
                next_char; if c/='x' then return neg; end if;
                next_char; if c/='p' then raise syntax_error; end if;
                next_char; return exp;

            when others =>
                next_char; return neg;
        end case;
    end scan_id;

    function scan_num return natural is 
        s: unbounded_string;
    begin
        while '0'<=c and c<='9' loop
            append(s, c);
            next_char;
        end loop;
        return natural(integer'value(to_string(s)));
    exception
        when constraint_error => raise syntax_error;
    end scan_num;

    procedure read_e(path: in String; e: out expression) is
    begin
        -- Set up global variables in order to read from file a non rule expression.
        open(f, In_File, path);
        reading_rule:= false;
        next_char; 

        -- Read expression and check for errors
        read_expr(e);
        if c/=';' then raise syntax_error; end if;

        close(f);
    exception
        when syntax_error => e:= b_null; close(f); raise;
    end read_e;

    procedure read_rule(e1, e2: out expression) is
    begin
        -- Set up global variables in order to read from stdin a rule expression.
        reading_rule:= true;
        next_char; 
        
        -- Read first rule expression
        read_expr(e1);
        -- Check for errors
        if c/=' ' and c/='=' then raise syntax_error; end if;

        -- Read gap (plus '=') between expressions
        while c=' ' loop next_char; end loop;
        if c/='=' then raise syntax_error; end if;
        next_char; while c=' ' loop next_char; end loop;

        -- Read second rule expression
        read_expr(e2);
        -- Check for errors
        if c/=';' then raise syntax_error; end if;

    exception
        when syntax_error => e1:= b_null; e2:= b_null; raise;
    end read_rule;

end utilities;
