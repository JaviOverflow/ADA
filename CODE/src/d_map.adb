package body d_map is

    -- Empty the mapping
    procedure empty (m: out map) is
        e: existence renames m.e;
    begin
        for i in key loop e(i):= false; end loop;
    end empty;

    -- Associate an item to a certain key
    procedure put (m: in out map; k: in key; x: in item) is
        e: existence renames m.e;
        c: contents  renames m.c;
    begin
        if e(k) then if c(k)/=x then raise already_exists; end if;
                else e(k):= true; c(k):= x;
        end if;
    end put;

    -- Get the item related to a key
    function get (m: in map; k: in key) return item is
        e: existence renames m.e;
        c: contents  renames m.c;
    begin
        if e(k) then return c(k);
                else raise does_not_exists;
        end if;
    end get;
end d_map;
