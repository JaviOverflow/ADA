with Ada.text_io, global_declarations;
use Ada.text_io, global_declarations, global_declarations.my_dexpr;

package utilities is

    syntax_error: exception;

    procedure read_e(path: in String; e: out expression);
    procedure read_rule(e1, e2: out expression);
end utilities;
