with d_expr;

package global_declarations is
    max_expressions: constant natural:= 300;
    max_vertical_size_expression: constant natural:= 20;
    max_rules: constant natural:= 20;
    max_expression_changes: constant natural:= 30;

    package my_dexpr is new d_expr(max_expressions, max_vertical_size_expression);
end global_declarations;
