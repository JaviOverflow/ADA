echo "" > $1
for f in *.ads
do 
    echo "Processing $f file.." 
    echo '$ \pagebreak $' >> $1
    echo '\section{'$f'}' >> $1
    echo '\begin{lstlisting}[ basicstyle=\small, numbers=left, frame=single, tabsize=3, backgroundcolor=\color{mygray}]' >> $1
    cat $f >> $1
    echo '\end{lstlisting}' >> $1

    f=`echo $f | sed "s/ads/adb/g"`
    if [ -f $f ]
    then
        echo "Processing $f file.." 
        echo '$ \pagebreak $' >> $1
        echo '\section{'$f'}' >> $1
        echo '\begin{lstlisting}[ basicstyle=\small, numbers=left, frame=single, tabsize=3, backgroundcolor=\color{mygray}]' >> $1
        cat $f >> $1
        echo '\end{lstlisting}' >> $1
    fi
done
