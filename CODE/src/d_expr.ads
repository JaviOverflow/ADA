with d_stack, d_map;

generic
    n: natural;
    max_vertical_size_expression: natural; -- Height of the tree used
package d_expr is
    type expression_type    is (e_null, e_const, e_var, e_un, e_bin, e_gen);
    type un_op              is (neg, sin, cos, exp, log);
    type bin_op             is (add, sub, prod, quot, power);

    type expression         is private;

    space_overflow: exception;

    -- Operations to build expressions:
    function b_null return expression;
    function b_constant (n: in natural) return expression;
    function b_var (x: in character) return expression;
    function b_gen (x: in character) return expression;
    function b_un_op (op: in un_op; esb: in expression) return expression;
    function b_bin_op (op: in bin_op; e1, e2: in expression) return expression;

    -- Operations to get components from expressions:
    function e_type (e: in expression) return expression_type;
    function g_const (e: in expression) return natural;
    function g_var (e: in expression) return character;
    function g_gen (e: in expression) return character;
    procedure g_un (e: in expression; op: out un_op; esb: out expression);
    procedure g_bin (e: in expression; op: out bin_op; esb1, esb2: out expression);

    -- Operations to iterate over a expression
    type iterator is limited private;

    bad_use: exception;

    procedure first (e: in expression; it: out iterator);
    function is_valid (it: in iterator) return boolean;
    procedure get (it: in iterator; e: out expression);
    procedure succ (it: in out iterator);

    function to_string (it: in iterator) return string;
    function to_string (e: in expression) return string;

    -- Operations to build new expression from generics
    function build_expression_from_generics(gen1, gen2: in expression; 
        it: in out iterator) return expression;

    function operate(it: in out iterator) return expression;
private
    max_e: constant natural:= n;
    max_e_height: constant natural:= max_vertical_size_expression;

    b: constant natural:= natural(float(max_e)*1.25);

    type expression is new natural range 0..b-1;

    type direction is (left, right);

    type path is 
        record
            e: expression;
            d: direction;
        end record;

    package path_dstack is new d_stack(path, max_e_height); use path_dstack;
    package my_dmap is new d_map(character, expression); use my_dmap;

    type iterator is
        record
            e: expression;
            path: stack;
            sub_e: expression;
        end record;
end d_expr;
