pragma Ada_95;
with System;
package ada_main is
   pragma Warnings (Off);

   gnat_argc : Integer;
   gnat_argv : System.Address;
   gnat_envp : System.Address;

   pragma Import (C, gnat_argc);
   pragma Import (C, gnat_argv);
   pragma Import (C, gnat_envp);

   gnat_exit_status : Integer;
   pragma Import (C, gnat_exit_status);

   GNAT_Version : constant String :=
                    "GNAT Version: GPL 2014 (20140331)" & ASCII.NUL;
   pragma Export (C, GNAT_Version, "__gnat_version");

   Ada_Main_Program_Name : constant String := "_ada_main" & ASCII.NUL;
   pragma Export (C, Ada_Main_Program_Name, "__gnat_ada_main_program_name");

   procedure adainit;
   pragma Export (C, adainit, "adainit");

   procedure adafinal;
   pragma Export (C, adafinal, "adafinal");

   function main
     (argc : Integer;
      argv : System.Address;
      envp : System.Address)
      return Integer;
   pragma Export (C, main, "main");

   type Version_32 is mod 2 ** 32;
   u00001 : constant Version_32 := 16#92c7b809#;
   pragma Export (C, u00001, "mainB");
   u00002 : constant Version_32 := 16#fbff4c67#;
   pragma Export (C, u00002, "system__standard_libraryB");
   u00003 : constant Version_32 := 16#f1136198#;
   pragma Export (C, u00003, "system__standard_libraryS");
   u00004 : constant Version_32 := 16#3ffc8e18#;
   pragma Export (C, u00004, "adaS");
   u00005 : constant Version_32 := 16#12c24a43#;
   pragma Export (C, u00005, "ada__charactersS");
   u00006 : constant Version_32 := 16#4b7bb96a#;
   pragma Export (C, u00006, "ada__characters__latin_1S");
   u00007 : constant Version_32 := 16#74c12fa4#;
   pragma Export (C, u00007, "ada__command_lineB");
   u00008 : constant Version_32 := 16#d59e21a4#;
   pragma Export (C, u00008, "ada__command_lineS");
   u00009 : constant Version_32 := 16#f2f2d889#;
   pragma Export (C, u00009, "systemS");
   u00010 : constant Version_32 := 16#c96bf39e#;
   pragma Export (C, u00010, "system__secondary_stackB");
   u00011 : constant Version_32 := 16#599317e0#;
   pragma Export (C, u00011, "system__secondary_stackS");
   u00012 : constant Version_32 := 16#c8ed38da#;
   pragma Export (C, u00012, "system__parametersB");
   u00013 : constant Version_32 := 16#f428403b#;
   pragma Export (C, u00013, "system__parametersS");
   u00014 : constant Version_32 := 16#daf76b33#;
   pragma Export (C, u00014, "system__soft_linksB");
   u00015 : constant Version_32 := 16#b82dbdbb#;
   pragma Export (C, u00015, "system__soft_linksS");
   u00016 : constant Version_32 := 16#342acf94#;
   pragma Export (C, u00016, "ada__exceptionsB");
   u00017 : constant Version_32 := 16#c71ae72a#;
   pragma Export (C, u00017, "ada__exceptionsS");
   u00018 : constant Version_32 := 16#032105bb#;
   pragma Export (C, u00018, "ada__exceptions__last_chance_handlerB");
   u00019 : constant Version_32 := 16#2b293877#;
   pragma Export (C, u00019, "ada__exceptions__last_chance_handlerS");
   u00020 : constant Version_32 := 16#393398c1#;
   pragma Export (C, u00020, "system__exception_tableB");
   u00021 : constant Version_32 := 16#5cebbe9c#;
   pragma Export (C, u00021, "system__exception_tableS");
   u00022 : constant Version_32 := 16#ce4af020#;
   pragma Export (C, u00022, "system__exceptionsB");
   u00023 : constant Version_32 := 16#9a91b57f#;
   pragma Export (C, u00023, "system__exceptionsS");
   u00024 : constant Version_32 := 16#2652ec14#;
   pragma Export (C, u00024, "system__exceptions__machineS");
   u00025 : constant Version_32 := 16#b895431d#;
   pragma Export (C, u00025, "system__exceptions_debugB");
   u00026 : constant Version_32 := 16#4110c137#;
   pragma Export (C, u00026, "system__exceptions_debugS");
   u00027 : constant Version_32 := 16#570325c8#;
   pragma Export (C, u00027, "system__img_intB");
   u00028 : constant Version_32 := 16#f029384b#;
   pragma Export (C, u00028, "system__img_intS");
   u00029 : constant Version_32 := 16#39a03df9#;
   pragma Export (C, u00029, "system__storage_elementsB");
   u00030 : constant Version_32 := 16#df31928d#;
   pragma Export (C, u00030, "system__storage_elementsS");
   u00031 : constant Version_32 := 16#ff5c7695#;
   pragma Export (C, u00031, "system__tracebackB");
   u00032 : constant Version_32 := 16#daf647d4#;
   pragma Export (C, u00032, "system__tracebackS");
   u00033 : constant Version_32 := 16#8c33a517#;
   pragma Export (C, u00033, "system__wch_conB");
   u00034 : constant Version_32 := 16#e98ffa5b#;
   pragma Export (C, u00034, "system__wch_conS");
   u00035 : constant Version_32 := 16#9721e840#;
   pragma Export (C, u00035, "system__wch_stwB");
   u00036 : constant Version_32 := 16#c49ed65a#;
   pragma Export (C, u00036, "system__wch_stwS");
   u00037 : constant Version_32 := 16#9b29844d#;
   pragma Export (C, u00037, "system__wch_cnvB");
   u00038 : constant Version_32 := 16#e63840a8#;
   pragma Export (C, u00038, "system__wch_cnvS");
   u00039 : constant Version_32 := 16#69adb1b9#;
   pragma Export (C, u00039, "interfacesS");
   u00040 : constant Version_32 := 16#ece6fdb6#;
   pragma Export (C, u00040, "system__wch_jisB");
   u00041 : constant Version_32 := 16#66485989#;
   pragma Export (C, u00041, "system__wch_jisS");
   u00042 : constant Version_32 := 16#8cb17bcd#;
   pragma Export (C, u00042, "system__traceback_entriesB");
   u00043 : constant Version_32 := 16#47e3b81b#;
   pragma Export (C, u00043, "system__traceback_entriesS");
   u00044 : constant Version_32 := 16#41837d1e#;
   pragma Export (C, u00044, "system__stack_checkingB");
   u00045 : constant Version_32 := 16#7c4db361#;
   pragma Export (C, u00045, "system__stack_checkingS");
   u00046 : constant Version_32 := 16#f64b89a4#;
   pragma Export (C, u00046, "ada__integer_text_ioB");
   u00047 : constant Version_32 := 16#f1daf268#;
   pragma Export (C, u00047, "ada__integer_text_ioS");
   u00048 : constant Version_32 := 16#1ac8b3b4#;
   pragma Export (C, u00048, "ada__text_ioB");
   u00049 : constant Version_32 := 16#17a49c57#;
   pragma Export (C, u00049, "ada__text_ioS");
   u00050 : constant Version_32 := 16#1b5643e2#;
   pragma Export (C, u00050, "ada__streamsB");
   u00051 : constant Version_32 := 16#2564c958#;
   pragma Export (C, u00051, "ada__streamsS");
   u00052 : constant Version_32 := 16#db5c917c#;
   pragma Export (C, u00052, "ada__io_exceptionsS");
   u00053 : constant Version_32 := 16#034d7998#;
   pragma Export (C, u00053, "ada__tagsB");
   u00054 : constant Version_32 := 16#ce72c228#;
   pragma Export (C, u00054, "ada__tagsS");
   u00055 : constant Version_32 := 16#c3335bfd#;
   pragma Export (C, u00055, "system__htableB");
   u00056 : constant Version_32 := 16#76306b63#;
   pragma Export (C, u00056, "system__htableS");
   u00057 : constant Version_32 := 16#089f5cd0#;
   pragma Export (C, u00057, "system__string_hashB");
   u00058 : constant Version_32 := 16#d46e001d#;
   pragma Export (C, u00058, "system__string_hashS");
   u00059 : constant Version_32 := 16#a3f44a26#;
   pragma Export (C, u00059, "system__unsigned_typesS");
   u00060 : constant Version_32 := 16#1e25d3f1#;
   pragma Export (C, u00060, "system__val_lluB");
   u00061 : constant Version_32 := 16#d9061d54#;
   pragma Export (C, u00061, "system__val_lluS");
   u00062 : constant Version_32 := 16#27b600b2#;
   pragma Export (C, u00062, "system__val_utilB");
   u00063 : constant Version_32 := 16#5e526e77#;
   pragma Export (C, u00063, "system__val_utilS");
   u00064 : constant Version_32 := 16#d1060688#;
   pragma Export (C, u00064, "system__case_utilB");
   u00065 : constant Version_32 := 16#d6fbb15e#;
   pragma Export (C, u00065, "system__case_utilS");
   u00066 : constant Version_32 := 16#9f23726e#;
   pragma Export (C, u00066, "interfaces__c_streamsB");
   u00067 : constant Version_32 := 16#bb1012c3#;
   pragma Export (C, u00067, "interfaces__c_streamsS");
   u00068 : constant Version_32 := 16#d82965ac#;
   pragma Export (C, u00068, "system__crtlS");
   u00069 : constant Version_32 := 16#967994fc#;
   pragma Export (C, u00069, "system__file_ioB");
   u00070 : constant Version_32 := 16#4e02348f#;
   pragma Export (C, u00070, "system__file_ioS");
   u00071 : constant Version_32 := 16#b7ab275c#;
   pragma Export (C, u00071, "ada__finalizationB");
   u00072 : constant Version_32 := 16#19f764ca#;
   pragma Export (C, u00072, "ada__finalizationS");
   u00073 : constant Version_32 := 16#95817ed8#;
   pragma Export (C, u00073, "system__finalization_rootB");
   u00074 : constant Version_32 := 16#bd00ab19#;
   pragma Export (C, u00074, "system__finalization_rootS");
   u00075 : constant Version_32 := 16#769e25e6#;
   pragma Export (C, u00075, "interfaces__cB");
   u00076 : constant Version_32 := 16#3b563890#;
   pragma Export (C, u00076, "interfaces__cS");
   u00077 : constant Version_32 := 16#d0432c8d#;
   pragma Export (C, u00077, "system__img_enum_newB");
   u00078 : constant Version_32 := 16#93bede49#;
   pragma Export (C, u00078, "system__img_enum_newS");
   u00079 : constant Version_32 := 16#0f6191e4#;
   pragma Export (C, u00079, "system__os_libB");
   u00080 : constant Version_32 := 16#94c13856#;
   pragma Export (C, u00080, "system__os_libS");
   u00081 : constant Version_32 := 16#1a817b8e#;
   pragma Export (C, u00081, "system__stringsB");
   u00082 : constant Version_32 := 16#8c4dc9ef#;
   pragma Export (C, u00082, "system__stringsS");
   u00083 : constant Version_32 := 16#3d557957#;
   pragma Export (C, u00083, "system__file_control_blockS");
   u00084 : constant Version_32 := 16#a4371844#;
   pragma Export (C, u00084, "system__finalization_mastersB");
   u00085 : constant Version_32 := 16#86e4f1c9#;
   pragma Export (C, u00085, "system__finalization_mastersS");
   u00086 : constant Version_32 := 16#57a37a42#;
   pragma Export (C, u00086, "system__address_imageB");
   u00087 : constant Version_32 := 16#531e45b3#;
   pragma Export (C, u00087, "system__address_imageS");
   u00088 : constant Version_32 := 16#7268f812#;
   pragma Export (C, u00088, "system__img_boolB");
   u00089 : constant Version_32 := 16#072ba962#;
   pragma Export (C, u00089, "system__img_boolS");
   u00090 : constant Version_32 := 16#d7aac20c#;
   pragma Export (C, u00090, "system__ioB");
   u00091 : constant Version_32 := 16#6cb02fc6#;
   pragma Export (C, u00091, "system__ioS");
   u00092 : constant Version_32 := 16#6d4d969a#;
   pragma Export (C, u00092, "system__storage_poolsB");
   u00093 : constant Version_32 := 16#07a95f0d#;
   pragma Export (C, u00093, "system__storage_poolsS");
   u00094 : constant Version_32 := 16#e34550ca#;
   pragma Export (C, u00094, "system__pool_globalB");
   u00095 : constant Version_32 := 16#c88d2d16#;
   pragma Export (C, u00095, "system__pool_globalS");
   u00096 : constant Version_32 := 16#d6f619bb#;
   pragma Export (C, u00096, "system__memoryB");
   u00097 : constant Version_32 := 16#ab8fbebd#;
   pragma Export (C, u00097, "system__memoryS");
   u00098 : constant Version_32 := 16#7b002481#;
   pragma Export (C, u00098, "system__storage_pools__subpoolsB");
   u00099 : constant Version_32 := 16#e3b008dc#;
   pragma Export (C, u00099, "system__storage_pools__subpoolsS");
   u00100 : constant Version_32 := 16#63f11652#;
   pragma Export (C, u00100, "system__storage_pools__subpools__finalizationB");
   u00101 : constant Version_32 := 16#fe2f4b3a#;
   pragma Export (C, u00101, "system__storage_pools__subpools__finalizationS");
   u00102 : constant Version_32 := 16#f6fdca1c#;
   pragma Export (C, u00102, "ada__text_io__integer_auxB");
   u00103 : constant Version_32 := 16#b9793d30#;
   pragma Export (C, u00103, "ada__text_io__integer_auxS");
   u00104 : constant Version_32 := 16#e0da2b08#;
   pragma Export (C, u00104, "ada__text_io__generic_auxB");
   u00105 : constant Version_32 := 16#a6c327d3#;
   pragma Export (C, u00105, "ada__text_io__generic_auxS");
   u00106 : constant Version_32 := 16#d48b4eeb#;
   pragma Export (C, u00106, "system__img_biuB");
   u00107 : constant Version_32 := 16#65d6c26b#;
   pragma Export (C, u00107, "system__img_biuS");
   u00108 : constant Version_32 := 16#2b864520#;
   pragma Export (C, u00108, "system__img_llbB");
   u00109 : constant Version_32 := 16#2411d295#;
   pragma Export (C, u00109, "system__img_llbS");
   u00110 : constant Version_32 := 16#9777733a#;
   pragma Export (C, u00110, "system__img_lliB");
   u00111 : constant Version_32 := 16#e3bd8d58#;
   pragma Export (C, u00111, "system__img_lliS");
   u00112 : constant Version_32 := 16#c2d63ebb#;
   pragma Export (C, u00112, "system__img_llwB");
   u00113 : constant Version_32 := 16#8d7df103#;
   pragma Export (C, u00113, "system__img_llwS");
   u00114 : constant Version_32 := 16#8ed53197#;
   pragma Export (C, u00114, "system__img_wiuB");
   u00115 : constant Version_32 := 16#0b9745f9#;
   pragma Export (C, u00115, "system__img_wiuS");
   u00116 : constant Version_32 := 16#f8f38c17#;
   pragma Export (C, u00116, "system__val_intB");
   u00117 : constant Version_32 := 16#ba57f2b6#;
   pragma Export (C, u00117, "system__val_intS");
   u00118 : constant Version_32 := 16#4266b2a8#;
   pragma Export (C, u00118, "system__val_unsB");
   u00119 : constant Version_32 := 16#b35ca71d#;
   pragma Export (C, u00119, "system__val_unsS");
   u00120 : constant Version_32 := 16#e892b88e#;
   pragma Export (C, u00120, "system__val_lliB");
   u00121 : constant Version_32 := 16#68d63e29#;
   pragma Export (C, u00121, "system__val_lliS");
   u00122 : constant Version_32 := 16#af50e98f#;
   pragma Export (C, u00122, "ada__stringsS");
   u00123 : constant Version_32 := 16#261c554b#;
   pragma Export (C, u00123, "ada__strings__unboundedB");
   u00124 : constant Version_32 := 16#e303cf90#;
   pragma Export (C, u00124, "ada__strings__unboundedS");
   u00125 : constant Version_32 := 16#6da9e383#;
   pragma Export (C, u00125, "ada__strings__searchB");
   u00126 : constant Version_32 := 16#c1ab8667#;
   pragma Export (C, u00126, "ada__strings__searchS");
   u00127 : constant Version_32 := 16#e2ea8656#;
   pragma Export (C, u00127, "ada__strings__mapsB");
   u00128 : constant Version_32 := 16#1e526bec#;
   pragma Export (C, u00128, "ada__strings__mapsS");
   u00129 : constant Version_32 := 16#9a74a760#;
   pragma Export (C, u00129, "system__bit_opsB");
   u00130 : constant Version_32 := 16#0765e3a3#;
   pragma Export (C, u00130, "system__bit_opsS");
   u00131 : constant Version_32 := 16#5b9edcc4#;
   pragma Export (C, u00131, "system__compare_array_unsigned_8B");
   u00132 : constant Version_32 := 16#5bf1a904#;
   pragma Export (C, u00132, "system__compare_array_unsigned_8S");
   u00133 : constant Version_32 := 16#5f72f755#;
   pragma Export (C, u00133, "system__address_operationsB");
   u00134 : constant Version_32 := 16#e1fe66ba#;
   pragma Export (C, u00134, "system__address_operationsS");
   u00135 : constant Version_32 := 16#afc64758#;
   pragma Export (C, u00135, "system__atomic_countersB");
   u00136 : constant Version_32 := 16#3f8e4c43#;
   pragma Export (C, u00136, "system__atomic_countersS");
   u00137 : constant Version_32 := 16#ffe20862#;
   pragma Export (C, u00137, "system__stream_attributesB");
   u00138 : constant Version_32 := 16#e5402c91#;
   pragma Export (C, u00138, "system__stream_attributesS");
   u00139 : constant Version_32 := 16#c50a16d0#;
   pragma Export (C, u00139, "d_rulesB");
   u00140 : constant Version_32 := 16#4d110a56#;
   pragma Export (C, u00140, "d_rulesS");
   u00141 : constant Version_32 := 16#9b7c53e0#;
   pragma Export (C, u00141, "global_declarationsS");
   u00142 : constant Version_32 := 16#e5480ede#;
   pragma Export (C, u00142, "ada__strings__fixedB");
   u00143 : constant Version_32 := 16#a86b22b3#;
   pragma Export (C, u00143, "ada__strings__fixedS");
   u00144 : constant Version_32 := 16#25e3da6b#;
   pragma Export (C, u00144, "d_exprB");
   u00145 : constant Version_32 := 16#0bc34a03#;
   pragma Export (C, u00145, "d_exprS");
   u00146 : constant Version_32 := 16#edf8db1f#;
   pragma Export (C, u00146, "d_mapB");
   u00147 : constant Version_32 := 16#43c5b3b1#;
   pragma Export (C, u00147, "d_mapS");
   u00148 : constant Version_32 := 16#d09a0aa0#;
   pragma Export (C, u00148, "d_stackB");
   u00149 : constant Version_32 := 16#edb895d7#;
   pragma Export (C, u00149, "d_stackS");
   u00150 : constant Version_32 := 16#b602a99c#;
   pragma Export (C, u00150, "system__exn_intB");
   u00151 : constant Version_32 := 16#b07ec4d5#;
   pragma Export (C, u00151, "system__exn_intS");
   u00152 : constant Version_32 := 16#23924001#;
   pragma Export (C, u00152, "utilitiesB");
   u00153 : constant Version_32 := 16#ab438ecf#;
   pragma Export (C, u00153, "utilitiesS");
   u00154 : constant Version_32 := 16#fd83e873#;
   pragma Export (C, u00154, "system__concat_2B");
   u00155 : constant Version_32 := 16#f0520f59#;
   pragma Export (C, u00155, "system__concat_2S");
   u00156 : constant Version_32 := 16#2b70b149#;
   pragma Export (C, u00156, "system__concat_3B");
   u00157 : constant Version_32 := 16#f982842c#;
   pragma Export (C, u00157, "system__concat_3S");
   u00158 : constant Version_32 := 16#932a4690#;
   pragma Export (C, u00158, "system__concat_4B");
   u00159 : constant Version_32 := 16#8c96f3a9#;
   pragma Export (C, u00159, "system__concat_4S");
   u00160 : constant Version_32 := 16#608e2cd1#;
   pragma Export (C, u00160, "system__concat_5B");
   u00161 : constant Version_32 := 16#75ac9ba7#;
   pragma Export (C, u00161, "system__concat_5S");
   u00162 : constant Version_32 := 16#a83b7c85#;
   pragma Export (C, u00162, "system__concat_6B");
   u00163 : constant Version_32 := 16#2035f53b#;
   pragma Export (C, u00163, "system__concat_6S");
   --  BEGIN ELABORATION ORDER
   --  ada%s
   --  ada.characters%s
   --  ada.characters.latin_1%s
   --  ada.command_line%s
   --  interfaces%s
   --  system%s
   --  system.address_operations%s
   --  system.address_operations%b
   --  system.atomic_counters%s
   --  system.atomic_counters%b
   --  system.case_util%s
   --  system.case_util%b
   --  system.exn_int%s
   --  system.exn_int%b
   --  system.htable%s
   --  system.img_bool%s
   --  system.img_bool%b
   --  system.img_enum_new%s
   --  system.img_enum_new%b
   --  system.img_int%s
   --  system.img_int%b
   --  system.img_lli%s
   --  system.img_lli%b
   --  system.io%s
   --  system.io%b
   --  system.parameters%s
   --  system.parameters%b
   --  system.crtl%s
   --  interfaces.c_streams%s
   --  interfaces.c_streams%b
   --  system.standard_library%s
   --  system.exceptions_debug%s
   --  system.exceptions_debug%b
   --  system.storage_elements%s
   --  system.storage_elements%b
   --  system.stack_checking%s
   --  system.stack_checking%b
   --  system.string_hash%s
   --  system.string_hash%b
   --  system.htable%b
   --  system.strings%s
   --  system.strings%b
   --  system.os_lib%s
   --  system.traceback_entries%s
   --  system.traceback_entries%b
   --  ada.exceptions%s
   --  system.soft_links%s
   --  system.unsigned_types%s
   --  system.img_biu%s
   --  system.img_biu%b
   --  system.img_llb%s
   --  system.img_llb%b
   --  system.img_llw%s
   --  system.img_llw%b
   --  system.img_wiu%s
   --  system.img_wiu%b
   --  system.val_int%s
   --  system.val_lli%s
   --  system.val_llu%s
   --  system.val_uns%s
   --  system.val_util%s
   --  system.val_util%b
   --  system.val_uns%b
   --  system.val_llu%b
   --  system.val_lli%b
   --  system.val_int%b
   --  system.wch_con%s
   --  system.wch_con%b
   --  system.wch_cnv%s
   --  system.wch_jis%s
   --  system.wch_jis%b
   --  system.wch_cnv%b
   --  system.wch_stw%s
   --  system.wch_stw%b
   --  ada.exceptions.last_chance_handler%s
   --  ada.exceptions.last_chance_handler%b
   --  system.address_image%s
   --  system.bit_ops%s
   --  system.bit_ops%b
   --  system.compare_array_unsigned_8%s
   --  system.compare_array_unsigned_8%b
   --  system.concat_2%s
   --  system.concat_2%b
   --  system.concat_3%s
   --  system.concat_3%b
   --  system.concat_4%s
   --  system.concat_4%b
   --  system.concat_5%s
   --  system.concat_5%b
   --  system.concat_6%s
   --  system.concat_6%b
   --  system.exception_table%s
   --  system.exception_table%b
   --  ada.io_exceptions%s
   --  ada.strings%s
   --  ada.strings.maps%s
   --  ada.strings.fixed%s
   --  ada.strings.search%s
   --  ada.strings.search%b
   --  ada.tags%s
   --  ada.streams%s
   --  ada.streams%b
   --  interfaces.c%s
   --  system.exceptions%s
   --  system.exceptions%b
   --  system.exceptions.machine%s
   --  system.finalization_root%s
   --  system.finalization_root%b
   --  ada.finalization%s
   --  ada.finalization%b
   --  system.storage_pools%s
   --  system.storage_pools%b
   --  system.finalization_masters%s
   --  system.storage_pools.subpools%s
   --  system.storage_pools.subpools.finalization%s
   --  system.storage_pools.subpools.finalization%b
   --  system.stream_attributes%s
   --  system.stream_attributes%b
   --  system.memory%s
   --  system.memory%b
   --  system.standard_library%b
   --  system.pool_global%s
   --  system.pool_global%b
   --  system.file_control_block%s
   --  system.file_io%s
   --  system.secondary_stack%s
   --  system.file_io%b
   --  system.storage_pools.subpools%b
   --  system.finalization_masters%b
   --  interfaces.c%b
   --  ada.tags%b
   --  ada.strings.fixed%b
   --  ada.strings.maps%b
   --  system.soft_links%b
   --  system.os_lib%b
   --  ada.command_line%b
   --  system.secondary_stack%b
   --  system.address_image%b
   --  ada.strings.unbounded%s
   --  ada.strings.unbounded%b
   --  system.traceback%s
   --  ada.exceptions%b
   --  system.traceback%b
   --  ada.text_io%s
   --  ada.text_io%b
   --  ada.text_io.generic_aux%s
   --  ada.text_io.generic_aux%b
   --  ada.text_io.integer_aux%s
   --  ada.text_io.integer_aux%b
   --  ada.integer_text_io%s
   --  ada.integer_text_io%b
   --  d_map%s
   --  d_map%b
   --  d_stack%s
   --  d_stack%b
   --  d_expr%s
   --  d_expr%b
   --  global_declarations%s
   --  utilities%s
   --  utilities%b
   --  d_rules%s
   --  d_rules%b
   --  main%b
   --  END ELABORATION ORDER


end ada_main;
