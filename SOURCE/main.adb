-- Input / Output imports
with ada.text_io, ada.characters.latin_1, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

-- Expression treatment packages
with global_declarations, d_iterable_expr, utilities;
use global_declarations.my_dexpr, d_iterable_expr, utilities;

procedure main is
    -- Menu variables
    nl: string:= ada.characters.latin_1.CR & ada.characters.latin_1.LF;
    clear_screen: string:= ascii.esc & "[2J";
    x: character; -- character to hold user option
    nil: character; -- trash character

    -- Expression related variables
    e: expression;
    --rt: rules_table;

    -- Reads new expression
    procedure new_expression is
        file_path: string(1..128);
        nil: natural;
    begin
        put_line("Enter the path of the file containing the expression: ");
        get_line(file_path, nil);

        read_e(file_path, e);
        --empty(rt);
        put_line("Expression was succesfully read from file."
            & nl & "Old rules were discarted");
        put_line(nl & "Press enter to go back to menu...");
        get(nil);
    exception
        when syntax_error =>
            put_line("File does not contains a correct expression.");
            put_line(nl & "Press enter to go back to menu...");
            get(nil);
    end new_expression;

    -- Adds new rule
    procedure new_rule is
    begin
        put("Enter the desired rule: ");
        --put(rt);

    exception
        when syntax_error =>
            put_line("Rule does not have a correct syntax.");
            put_line(nl & "Press enter to go back to menu...");
            get(nil);
        when space_overflow =>
            put_line("No left space for more rules.");
            put_line(nl & "Press enter to go back to menu...");
            get(nil);
    end new_rule;
begin
    x:= '@';
    while x/='0' loop
        -- Output the menu info
        put(clear_screen);
        put_line("Enter one of the following numbers to choose an action:"
            & nl & "[1] New expression"
            & nl & "[2] New rule"
            & nl & "[3] Simplify the expression"
            & nl & "[4] Undo changes"
            & nl & "[0] Quit" & nl);

        -- Read the user input
        get(x);
        case x is
            when '0' => new_expression;
            when '1' => new_rule;
            when '3' => put_line("two selected");
            when '4' => put_line("third selected");
            when others => put_line("Please insert a correct option");
        end case;
    end loop;
end main;
