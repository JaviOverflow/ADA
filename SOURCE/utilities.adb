package body utilities is

    c: character; -- Next character to process
    f: file_type; -- File containing the expression
    reading_rule: boolean:= false; -- if true, uses b_gen for literals. Otherwise, b_var.

    procedure read_expr (e: out expression);
    procedure read_term (e: out expression);
    procedure read_factor (e: out expression);
    procedure read_factor1 (e: out expression);
    function scan_id   return un_op;
    function scan_num return natural;

    procedure next_char is
    begin
        if reading_rule then get(c); 
                        else get(f,c); 
        end if;
    end next_char;

    procedure read_expr(e: out expression) is
        neg_term: boolean;
        op:		  character;
        e1:		  expression;
    begin
        neg_term:= c='-'; if neg_term then next_char; end if;
        read_term(e);
        if neg_term then e:= b_un_op(neg, e); end if;
        while c='+' or c='-' loop
            op:= c; next_char; read_term(e1);
            if op='+' then e:= b_bin_op(add, e, e1);
                      else e:= b_bin_op(sub, e, e1);
            end if;
        end loop;
    end read_expr;

    procedure read_term(e: out expression) is
        op: character;
        e1: expression;
    begin
        read_factor(e);
        while c='*' or c='/' loop
            op:= c; next_char; read_factor(e1);
            if op ='*' then e:= b_bin_op(prod, e, e1);
                       else e:= b_bin_op(sub, e, e1);
            end if;
       end loop;
    end read_term;

    procedure read_factor(e: out expression) is 
        e1: expression;
    begin
        read_factor1(e);
        if c='^' then
            next_char; read_factor1(e1);
            e:= b_bin_op(power, e, e1);
        end if;
    end read_factor;

    procedure read_factor1(e: out expression) is
        t: un_op;
        x: character;
    begin
        if c='(' then
            next_char; read_expr(e);
            if c/=')' then raise syntax_error; end if;
            next_char;
        elsif 'a'<=c and c<='z' then
            x:= c;
            t:= scan_id; 
            if t=neg then 
                if reading_rule then e:= b_gen(x);
                                else e:= b_var(x); 
                end if;
            else
                if c/='(' then raise syntax_error; end if;
                next_char;
                read_expr(e);
                if c/=')' then raise syntax_error; end if;
                next_char;
                e:= b_un_op(t, e);
            end if;
        elsif '0'<=c and c<'9' then
            e:= b_constant(scan_num);
        else
            raise syntax_error;
        end if;
    end read_factor1;

    -- (neg, sin, cos, exp, ln)
    function scan_id return un_op is
        r: un_op;
    begin
        case c is
            when 'l' =>
                next_char;
                if c='n' then next_char; r:= ln; end if;

            when 'n' => 
                next_char;
                if c='e' then
                    next_char;
                    if c='g' then next_char; r:=neg; end if;
                end if;
                    
            when 's' =>
                next_char;
                if c='i' then
                    next_char;
                    if c='n' then next_char; r:=sin; end if;
                end if;

            when 'c' =>
                next_char;
                if c='o' then
                    next_char;
                    if c='s' then next_char; r:=cos; end if;
                end if;

            when 'e' => 
                next_char;
                if c='x' then
                    next_char;
                    if c='p' then next_char; r:=exp; end if;
                end if;
            when others =>
                null;
        end case;
        
        return r;
    end scan_id;

    function scan_num return natural is 
        -- Precondition: c is a value between 0 and 9
        x: integer;
        r: natural;
    begin
        r:= 0;
        x:= character'pos(c) - character'pos('0');
        while 0<=x and x<=9 loop
            r:= r*10 + natural(x);
            next_char; x:= character'pos(c) - character'pos('0');
        end loop;
        return r; -- Puede dar overflow (constraint_error)
    exception
        when constraint_error => raise syntax_error;
    end scan_num;

    procedure read_e(path: in String; e: out expression) is
    begin
        -- Set up global variables in order to read from file a non rule expression.
        open(f, In_File, path);
        reading_rule:= false;
        next_char; 

        -- Read expression and check for errors
        read_expr(e);
        if c/=';' then raise syntax_error; end if;

        close(f);
    exception
        when syntax_error => e:= b_null; close(f); raise;
    end read_e;

    procedure read_rule(e1, e2: out expression) is
    begin
        -- Set up global variables in order to read from stdin a rule expression.
        reading_rule:= true;
        next_char; 
        
        -- Read first rule expression
        read_expr(e1);
        -- Check for errors
        if c/=' ' or c/='=' then raise syntax_error; end if;

        -- Read gap (plus '=') between expressions
        while c=' ' loop next_char; end loop;
        if c/='=' then raise syntax_error; end if;
        next_char; while c=' ' loop next_char; end loop;

        -- Read second rule expression
        read_expr(e2);
        -- Check for errors
        if c/=';' then raise syntax_error; end if;

    exception
        when syntax_error => e1:= b_null; e2:= b_null; raise;
    end read_rule;

    -------------------- EXPRESSION ITERATOR FUNCTIONS ----------------------------

    -- Initializes iterator with a certain expression 'e'
    procedure first (e: in expression; it: out iterator) is
    begin
        -- Initialize output iterator
        it.e:= e;
        first(e, it.it);
    end first;

    -- Checks if iterator points to a valid subexpression 
    function is_valid (it: in iterator) return boolean;
    begin
        return is_valid(it.it);
    end is_valid;

    -- Changes the iterator state to the next subexpression
    procedure succ (it: in out iterator) is
    begin
        succ(it.it);
    end succ;

    -- Prints the expression related to the iterator 'it', highlighting with square brackets
    --  a certain subexpression (depending on iterator state) when 'hl_subexp' is true. If
    --  it's false, clean expression will be printed.
    procedure out_it (it: in iterator; hl_subexp: in boolean) is
        i: d_iterable_expr.iterator renames it.it;
        s: d_iterable_expr.expression_dstack.stack;
    begin
        empty(s); push(s, it.e);
        out_it0 (i, s, hl_subexp);
    end out_it;

    -- Auxiliar procedure for recursive calls
    procedure out_it0 (i: in d_iterable_expr.iterator; s: in out stack; hl_subexp: in boolean) is
        e: expression;
        hl: boolean; -- True when this is the expression to highlight

        -- Variables for unary and binary expression cases
        e1, e2: expression;
        opu: un_op; 
        opb: bin_op;
    begin
        -- Get the expression corresponding to this recursive level
        e:= top(s);

        -- Check if subexpression must be highlighted and it corresponds to this one
        if hl_subexp 
            then hl:= same_stack(i, s);
            else hl:= false;
        end if;
        if hl then put("["); end if;

        -- Print expression info
        case e_type(e) is
            when e_null =>
                put("Invalid expression");

            when e_const =>
                put(natural'image(g_const(e)));

            when e_var =>
                put(g_var(e));

            when e_gen =>
                put(g_gen(e));

            when e_un =>
                g_un(e, opu, e1);
                
                out_opu(opu); -- Print operand

                -- Print subexpression
                if e_type(e1)/=e_bin then put("("); end if;
                push(s, e1); out_it0(i, s);
                if e_type(e1)/=e_bin then put(")"); end if;

            when e_bin =>
                g_bin(e, opb, e1, e2);

                if not hl then put("("); end if;

                push(s, e1); out_it0(i, s);
                out_opb(opb);
                push(s,e2); out_it0(i, s);

                if not hl then put(")"); end if;
        end case;

        -- If this is the subexpression to highlight, output ending square bracket
        if hl then put("]"); end if;

        -- Pop subexpression since it's already processed
        pop(s);

    end out_it0;

    -- Prints the unary operand
    procedure out_opu (op: in un_op) is
    begin
        case op is
            when neg => put("-");
            when exp | sin | cos | ln | => put(un_op'Image(op));
        end case;
    end out_opu;

    -- Prints the binary operand
    procedure out_opb (op: in bin_op) is
    begin
        case op is
            when add => put("+");
            when sub => put("-");
            when prod => put("*");
            when quot => put("/");
            when power => put("^");
        end case;
    end out_opb;

end utilities;
