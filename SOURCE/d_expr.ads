generic
    n: natural;
package d_expr is
    type expression_type    is (e_null, e_const, e_var, e_un, e_bin, e_gen);
    type un_op              is (neg, sin, cos, exp, ln);
    type bin_op             is (add, sub, prod, quot, power);

    type expression         is private;

    space_overflow: exception;

    -- Operations to build expressions:
    function b_null return expression;
    function b_constant (n: in natural) return expression;
    function b_var (x: in character) return expression;
    function b_gen (x: in character) return expression;
    function b_un_op (op: in un_op; esb: in expression) return expression;
    function b_bin_op (op: in bin_op; e1, e2: in expression) return expression;

    -- Operations to get components from expressions:
    function e_type (e: in expression) return expression_type;
    function g_const (e: in expression) return natural;
    function g_var (e: in expression) return character;
    function g_gen (e: in expression) return character;
    procedure g_un (e: in expression; op: out un_op; esb: out expression);
    procedure g_bin (e: in expression; op: out bin_op; esb1, esb2: out expression);

private
    max_e: constant natural:= n;
    b: constant natural:= natural(float(max_e)*1.25);
    type expression is new natural range 0..b-1;
end d_expr;
