with d_expr;

package global_declarations is
    max_expressions: constant natural:= 50;

    package my_dexpr is new d_expr(max_expressions); use my_dexpr;
    --package my_dmap is new package d_map(character, expression);
    --package my_drules is new package d_rules(10);
end global_declarations;
