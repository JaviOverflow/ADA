with Ada.text_io, utilities, global_declarations;
use Ada.text_io, utilities, global_declarations;

generic
    n: natural;
package d_rules is
    type rules_table is limited private;

    space_overflow: exception;
    does_not_exists: exception;

    use my_dmap;

    procedure empty(rt: out rules_table);
    procedure put(rt: in out rules_table);
    procedure show(rt: in rules_table);

    function check_rule(r: in natural; e_it: in utilities.iterator; m: out map) return boolean;
    function apply_rule(r: in natural; e_it: in utilities.iterator; m: in map) return expression;

private
    max_r: constant natural:= n;

    type index is new natural range 0..max_r;

    type block is
        record
            e1: expression;
            e2: expression;
        end record;

    type mem_space is array(index range 1..max_r) of block;

    type rules_table is
        record
            m: mem_space;
            n: index;
        end record;
end d_rules; 
