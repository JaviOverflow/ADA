with Ada.text_io, d_iterable_expr, global_declarations;
use Ada.text_io, d_iterable_expr, global_declarations.my_dexpr;

package utilities is
    use d_iterable_expr;

    procedure read_e(path: in String; e: out expression);
    procedure read_rule(e1, e2: out expression);

    type iterator is limited private;

    procedure first (e: in expression; it: out iterator);
    function is_valid (it: in iterator) return boolean;
    procedure succ (it: in out iterator);
    procedure out_it (it: in iterator; hl_subexp: in boolean);

    syntax_error: exception;

private
    type iterator is
        record
            it: d_iterable_expr.iterator;
            e: expression;
        end record;
end utilities;
