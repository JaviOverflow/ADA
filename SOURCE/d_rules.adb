package body d_rules is

    procedure empty(rt: out rules_table) is
        n: index renames rt.n;
    begin
        n:= 0;
    end empty;

    procedure put(rt: in out rules_table) is
        n: index renames rt.n;
        m: mem_space renames rt.m;
        e1, e2: expression;
    begin
        read_rule(e1, e2);
        n:= n+1; m(n):= (e1, e2);
    exception
        when constraint_error => raise space_overflow;
    end put;

    procedure show(rt: in rules_table) is
        n: index renames rt.n;
        m: mem_space renames rt.m;
        i: index;
    begin
        if n=0 then
            put_line("There are no rules");
        else
            for i in 1..n loop
                put(index'image(i));
                put(".- ");
                show(m(i).e1);
                put(" = ");
                show(m(i).e2);
                new_line(1);
            end loop;
        end if;
    end show;

    function check_rule(r: in natural; e: in expression; m: out d_map) return boolean is
        e_exp, e_rul: expression;
    begin
        empty(m); -- Init the mapping
        return check_rule0(e_exp, m(index(r)).e1, m);
    end check_rule;

    function check_rule0(e_exp, e_rul: expression; m: out d_map) return boolean is
        ee1, ee2, er1, er2: expression;
        uop: un_op;
        bop: bin_op;
        r: boolean;
    begin
        r:= false;

        if e_exp=e_rul then
            case e_exp is
                when e_un =>
                    g_un(e_exp, uop, ee1);
                    g_un(e_rul, uop, er1);
                    if check_rule0(ee1, er1, m) then r:= true; end if;
                when e_bin =>
                    g_bin(e_exp, bop, ee1, ee2);
                    g_bin(e_rul, bop, er1, er2);
                    if check_rule0(ee1, er1, m) and check_rule0(ee2, er2, m) then r:= true; end if;
                when others =>
                    r:= true;
            end case;
        else
            if e_type(e_rul)=e_gen then put(m, e_rul, e_exp); r:= true; end if;
        end if;

        return r;
    exception
        when d_map.already_exists => return false;
    end check_rule0;

    function apply_rule(rt: in rules_table; r: in natural; it: in utilities.iterator; 
                        m: in map) return expression is
                        e_it: 
        m: mem_space renames rt.m;
        e: expression;
    begin
        e:= expression_from_generic(m(r).e2, m);
        build_expression_from_subexpression(e_it, e);
    end apply_rule;

end d_rules;
