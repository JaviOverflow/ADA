with global_declarations, d_stack;
use global_declarations.my_dexpr;

package d_iterable_expr is
    package expression_dstack is new d_stack(expression, 10); use expression_dstack;

    -- Comparision iterator
    function "=" (e1, e2: in expression) return boolean;

    -- Operations to iterate over a expression
    type iterator is limited private;

    procedure first (e: in expression; it: out iterator);
    function is_valid (it: in iterator) return boolean;
    procedure get (it: in iterator; e: out expression);
    procedure succ (it: in out iterator);
    function same_stack (it: in iterator; s: in expression_dstack.stack) return boolean;

private
    type direction is (left, right);

    type parent is 
        record
            e: expression;
            d: direction;
        end record;

    package parents_dstack is new d_stack(parent, 10); use parents_dstack;
    
    type iterator is
        record
            s: expression_dstack.stack;
            p: parents_dstack.stack;
        end record;
end d_iterable_expr;
