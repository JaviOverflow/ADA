package body d_iterable_expr is 

    -- Equal test
    function "=" (e1, e2: in expression) return boolean is
        b1: block renames et.m(e1).b;
        b2: block renames et.m(e2).b;
        r: boolean;
    begin
        r:= false;
        if b1.et=b2.et then
            case b1.et is
                when e_un =>
                    r:= b1.opun=b2.opun;
                when e_bin =>
                    r:= b1.opbin=b2.opbin;
                when others => 
                    r:= e1=e2;
            end case;
        end if;
        return r;
    end "=";

    -- Iterator procedures and functions
    procedure first (e: in expression; it: out iterator) is
        m: expression_space renames et.m;
        s: expression_space.stack renames it.s;
        p: parents_dstack.stack renames it.p;
        i: expression;
        enil: expression;
        unil: un_op;
        bnil: bin_op;
    begin
        -- Init stacks and check if expression is iterable
        empty(s); empty(p);
        if m(e).b.et/=e_null then
            -- Get the root expression
            i:= e;

            -- While it has left child
            while m(i).b.et=e_bin or m(i).b.et=e_un loop
                push(s, i); push(p, (i, left));
                case m(i).b.et is
                    when e_un =>
                        g_un(i, unil, i);
                    when e_bin =>
                        g_bin(i, bnil, i, enil);
                    when others =>
                        null;
                end case;
            end loop;

            -- Push leaf
            push(s, i);
        end if;
    end first;

    function is_valid (it: in iterator) return boolean is
        s: stack renames it.s;
    begin
        return not is_empty(s);
    end is_valid;

    procedure get (it: in iterator; e: out expression) is
        s: stack renames it.s;
    begin
        e:= top(s);
    exception
        when exp_stack.bad_use => raise;
    end get;
        

    procedure succ (it: in out iterator) is
        m: expression_space renames et.m;
        es: expression_dstack.stack renames it.s; -- expression stack
        i: expression;
        ps: parents_dstack.stack renames it.p; -- parents stack
        p: parent;
        enil: expression;
        unil: un_op;
        bnil: bin_op;
    begin
        i:= top(es); pop(es); 
        p:= top(ps); pop(ps);

        -- If it's an expression with second expression child
        if m(i).b.et=e_bin then

            -- Save parent expression with right direction
            push(ps, (p.e, right));

            -- Get the second child
            g_bin(i, bnil, enil, i);

            -- While it has left child
            while m(i).b.et=e_bin or m(i).b.et=e_un loop
                push(es, i); 
                push(ps, (i, left));
                case m(i).b.et is
                    when e_un =>
                        g_un(i, unil, i);
                    when e_bin =>
                        g_bin(i, bnil, i, enil);
                    when others =>
                        null;
                end case;
            end loop;

            -- Push leaf
            push(es, i);
        end if;
    exception
        when exp_stack.bad_use => raise;
    end succ;

    function same_stack (it: in iterator; s: in expression_dstack.stack) return boolean is
    begin
        return it.s=s;
    end same_stack;

end d_iterable_expr;
