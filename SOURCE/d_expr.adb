package body d_expr is
    subtype index is expression;

    type block(et: expression_type:= e_null) is
        record
            case et is
                when e_null =>
                    null;
                when e_const =>
                    val: natural;
                when e_var =>
                    var: character;
                when e_un =>
                    opun: un_op;
                    esb: index;
                when e_bin =>
                    opbin: bin_op;
                    esb1, esb2: index;
                when e_gen =>
                    gen: character;
                end case;
            end record;

    type block_state is (occupied, free);

    type cell is
        record
            b: block;
            bs: block_state;
        end record;

    type expression_space is array(index) of cell;

    type expression_table is
        record
            m: expression_space;
            n: natural;
        end record;

    et: expression_table;

    -- Hashing declarations
    type vector is array(1..9) of natural;
    m: constant natural:= 256;

    -- Hashing procedures and functions
    procedure append(v0: in natural; a: in out vector; n: in out natural) is
        v: natural;
    begin
        v:= v0;
        while v>0 loop
            n:= n+1; a(n):= v mod m; v:= v/m;
        end loop;
    end append;

    procedure to_vector(b: in block; a: out vector; n: out natural) is
    begin
        n:= 1;
        a(1):= expression_type'pos(b.et);
        case b.et is
            when e_null => null;
            when e_const => append(b.val, a, n);
            when e_var | e_gen => append(character'pos(b.var), a, n);
            when e_un =>
                a(1):= a(1)*8 + un_op'pos(b.opun);
                append(natural(b.esb), a, n);
            when e_bin =>
                a(1):= a(1)*8 + bin_op'pos(b.opbin);
                append(natural(b.esb1), a, n);
                append(natural(b.esb2), a, n);
        end case;
    end to_vector;

    procedure to_natural(a: in vector; n: in natural; t: out natural) is
        c: natural;
        k, l: integer;
        r: array(1..2*n) of natural;
    begin
        for i in 1..2*n loop r(i):= 0; end loop;
        k:= 2*n+1;
        for i in reverse 1..n loop
            k:=k-1; l:= k; c:= 0;
            for j in reverse 1..n loop
                c:= c + r(l) + a(i)*a(j);
                r(l):= c mod m; c:= c/m;
                l:= l-1;
            end loop;
            r(l):= c;
        end loop;
        t:= (r(n)*m + r(n+1));
    end to_natural;

    function hash(bl: in block) return index is
        a: vector; n: natural; t: natural;
    begin
        to_vector (bl, a, n);
        to_natural(a, n, t);
        return index(t mod b);
    end hash;

    procedure allocate(b: in block; e: out index) is
        m: expression_space renames et.m;
        n: natural renames et.n;
        i0, i: index; -- initial and current position
        na: index; -- number of attempts
    begin
        if n=max_e then raise space_overflow; end if;

        i0:= hash(b); na:= 0; i:= i0;
        while m(i).bs/=free and m(i).b/=b loop
            na:= na+1; i:= i0+na*na;
        end loop;

        if m(i).bs=free then m(i):= (b, occupied); end if;
        e:= i;
    end allocate;

    procedure prep_expr_space is
        m: expression_space renames et.m;
        n: natural renames et.n;
    begin
        for i in index'range loop m(i).bs:= free; end loop;
        n:= 0;
    end prep_expr_space;

    function b_null return expression is
        b: block; e: index;
    begin
        b:= block'(et=>e_null);
        allocate(b,e);
        return e;
    end b_null;

    function b_constant(n: in natural) return expression is
        b: block; e: index;
    begin
        b:= (e_const, n);
        allocate(b,e);
        return e;
    end b_constant;

    function b_var(x: in character) return expression is
        b: block; e: index;
    begin
        b:= (e_var,x);
        allocate(b,e);
        return e;
    end b_var;

    function b_gen(x: in character) return expression is
        b: block; e: index;
    begin
        b:= (e_gen,x);
        allocate(b,e);
        return e;
    end b_gen;

    function b_un_op (op: in un_op; esb: in expression) return expression is
        b: block; e: index;
    begin
        b:= (e_un,op,esb);
        allocate(b,e);
        return e;
    end b_un_op;

    function b_bin_op(op: in bin_op; e1,e2: in expression) return expression is
        b:block; e: index;
    begin
        b:= (e_bin,op,e1,e2);
        allocate(b,e);
        return e;
    end b_bin_op;

    function e_type(e: in expression) return expression_type is
       s: expression_space renames et.m;
    begin
    return s(e).b.et;
    end e_type;

    function g_const(e: in expression) return natural is
       s: expression_space renames et.m;
    begin
    return s(e).b.val;
    end g_const;

    function g_var(e: in expression) return character is
       s: expression_space renames et.m;
    begin
    return s(e).b.var;
    end g_var;

    function g_gen(e: in expression) return character is
       s: expression_space renames et.m;
    begin
    return s(e).b.gen;
    end g_gen;

    procedure g_un(e: in expression; op: out un_op; esb: out expression) is
       s: expression_space renames et.m;
    begin
    op:= s(e).b.opun;
    esb:= s(e).b.esb;
    end g_un;

    procedure g_bin(e: in expression; op: out bin_op; esb1, esb2: out expression) is
       s: expression_space renames et.m;
    begin
    op:= s(e).b.opbin;
    esb1:= s(e).b.esb1;
    esb2:= s(e).b.esb2;
    end g_bin;

begin
    prep_expr_space;
end d_expr;
